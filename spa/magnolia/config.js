import Home from '../templates/pages/Home';
import Basic from '../templates/pages/Basic';
import Event from '../templates/pages/Event';

import Headline from "../templates/components/Headline";
import Image from "../templates/components/Image";
import Paragraph from "../templates/components/Paragraph";
import Expander from "../templates/components/Expander";
import Listing from "../templates/components/List";
import Item from "../templates/components/Item";
import EventsList from "../templates/components/EventsList";
import EventDetail from "../templates/components/EventDetail";

import Form from "../templates/components/Form";
import FormInput from "../templates/components/FormInput";
import FormTextarea from "../templates/components/FormTextarea";
import FormNumber from "../templates/components/FormNumber";
import FormSelection from "../templates/components/FormSelection";
import FormCheckbox from "../templates/components/FormCheckbox";
import FormButton from "../templates/components/FormButton";


export const config = {
  componentMappings: {
    'spa-foundation:pages/home': Home,
    'spa-foundation:pages/basic': Basic,
    'spa-foundation:pages/event': Event,

    "spa-foundation:components/headline": Headline,
    "spa-foundation:components/image": Image,
    "spa-foundation:components/paragraph": Paragraph,
    "spa-foundation:components/expander": Expander,
    "spa-foundation:components/list": Listing,
    "spa-foundation:components/listItem": Item,
    "spa-foundation:components/eventsList": EventsList,
	  "spa-foundation:components/eventDetail": EventDetail,
    
    "formCustom:components/form": Form,
    "formCustom:components/formInput": FormInput,
    "formCustom:components/formTextarea": FormTextarea,
    "formCustom:components/formNumber": FormNumber,
    "formCustom:components/formSelection": FormSelection,
    "formCustom:components/formCheckbox": FormCheckbox,
    "formCustom:components/formButton": FormButton
    
    
  },
};


import axios from 'axios';

const headers = {
	Accept: 'application/json',
	'Content-Type': 'application/json'
};

const axiosClient = axios.create({
	headers
});


const nodeName = 'spa';


export async function getPage(context) {
	
	const pagesRes = await fetch(
		'http://localhost:8080/spa-project-webapp/.rest/delivery/pages/' +
		nodeName +
		context.resolvedUrl.replace(new RegExp('(.*' + nodeName + '|.html)', 'g'), '')
	);
	const page = await pagesRes.json();
	
	
	let templateDefinitions = {};
	if (context.query.mgnlPreview === 'false') {
		const templateDefinitionsRes = await fetch(
			'http://localhost:8080/spa-project-webapp/.rest/template-definitions/v1/' +
			page['mgnl:template']
		);
		
		templateDefinitions = await templateDefinitionsRes.json();
	}
	
	return {page, templateDefinitions};
}


export async function getAllMenus() {
	// 2
	const url = 'http://localhost:8080/spa-project-webapp/.rest/delivery/pagenav';
	const navItems = await axiosClient.get(url).then(function (response) {
		// handle success
		return response.data.results;
	})
	return navItems;
}


export async function getEvents() {
	const url = 'http://localhost:8080/spa-project-webapp/.rest/delivery/events';
	const Events = await axiosClient.get(url).then(function (response) {
		// handle success
		return response.data.results;
	})
	return Events;
}
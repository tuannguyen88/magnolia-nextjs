
import { Heading } from "@chakra-ui/react"

const Headline = (props) => {
  return (
    <Heading 
      as={props.headlineLevel} 
      className={`Headline ${props.headlineClass}`}
    >
      {props.text}
    </Heading>
  );
}
export default Headline;



import { convertBoolean } from '../../../services/util';
import {
  FormControl,
  FormLabel,
  NumberInput,
  InputGroup,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper
  
} from "@chakra-ui/react"

const FormEdit = (props) => {

  
  const id = "element" + props.metadata['@id'];
  const {label, require} = props;

  return (
    <FormControl id={id} mb={5}>
      {label &&
        <FormLabel fontWeight="normal">
          {label}
          {convertBoolean(require) && 
            <dfn title="required">*</dfn>
          }
        </FormLabel>
      }
      <InputGroup size="lg">
        <NumberInput 
          defaultValue={props.defaultValue*1} 
          min={props.min*1} 
          max={props.max*1}
          precision={props.precision??0}
          step={props.step*1}
        >
          <NumberInputField />
          <NumberInputStepper>
            <NumberIncrementStepper />
            <NumberDecrementStepper />
          </NumberInputStepper>
        </NumberInput>
      </InputGroup> 
    </FormControl>
  );
}

export default FormEdit;
import { FormControl, Button } from "@chakra-ui/react"

const formSubmit = (props) => {
  console.log(props)
  
  return (
    <FormControl mb={5}>
      <Button
        colorScheme="blue"
        type="submit"
        size="lg"
      >
        {props.buttonText}
      </Button>
    </FormControl>
  );
}

export default formSubmit;
import { convertBoolean } from '../../../services/util';
import { FormControl, FormLabel, Select } from "@chakra-ui/react"


const FormSelection = (props) => {

  const id = "element" + props.metadata['@id'];
  const {label, require, disable, readonly} = props;

  const dataNodes = props.options['@nodes']
  let options = [];

  dataNodes.map((x)=>{
    options.push(props.options[x])
  })

  return (
    <FormControl id={id} mb={5}>
      {label &&
        <FormLabel fontWeight="normal">
          {label}
          {convertBoolean(require) && 
            <dfn title="required">*</dfn>
          }
        </FormLabel>
      }
      <Select
        placeholder={props.placeholder}
        isRequired={convertBoolean(require)}
        isDisabled={convertBoolean(disable)}
        isReadOnly={convertBoolean(readonly)}
      >
       {options?.map(({ label, value, isDisable }) => {
            return (
              <option key={value} value={value} disabled={convertBoolean(isDisable)}>
                  {label}
              </option>
            )
        })}

      </Select>
    </FormControl>
  );
}

export default FormSelection;
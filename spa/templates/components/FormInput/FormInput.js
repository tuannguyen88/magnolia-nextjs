import {useState} from 'react';
import {convertBoolean} from '../../../services/util';
import {
    FormControl,
    FormLabel,
    Input,
    InputGroup,
    InputLeftAddon,
    InputRightAddon,
    InputRightElement,
    Button,

} from "@chakra-ui/react"

const FormEdit = (props) => {

    const [show, setShow] = useState(false);
    const handleClick = () => setShow(!show);

    const id = "element" + props.metadata['@id'];
    const {label, require, disable, readonly, autocomplete, maxLength} = props;


    const inputType = () => {
        switch (props.inputType) {
            case "password":
                return (
                    <>
                        <Input size="lg"
                               type={show ? "text" : "password"}
                               placeholder={props.placeholder}
                               isRequired={convertBoolean(require)}
                               isDisabled={convertBoolean(disable)}
                               isReadOnly={convertBoolean(readonly)}
                               autoComplete={autocomplete}
                        />
                        <InputRightElement width="4.5rem">
                            <Button h="1.75rem" size="sm" onClick={handleClick}>
                                {show ? "Hide" : "Show"}
                            </Button>
                        </InputRightElement>
                    </>
                );
            default:
                return (
                    <Input size="lg"
                           type={props.inputType}
                           placeholder={props.placeholder}
                           isRequired={convertBoolean(require)}
                           isDisabled={convertBoolean(disable)}
                           isReadOnly={convertBoolean(readonly)}
                           autoComplete={autocomplete}
                           maxLength={maxLength ? maxLength * 1 : ''}
                    />
                );
        }
    }

    return (
        <FormControl id={id} mb={5}>
            {label &&
            <FormLabel fontWeight="normal">
                {label}
                {convertBoolean(require) &&
                <dfn title="required">*</dfn>
                }
            </FormLabel>
            }
            <InputGroup size="lg">
                {props.leftAddon &&
                <InputLeftAddon children={props.leftAddon}/>
                }

                {inputType()}

                {props.rightAddon &&
                <InputRightAddon children={props.rightAddon}/>
                }

            </InputGroup>
        </FormControl>
    );
}

export default FormEdit;
import React from 'react';
import { ListItem } from "@chakra-ui/react"


const Item = (props) => {
  return (
    <ListItem dangerouslySetInnerHTML={{ __html: props.richText }}>
     
    </ListItem>
  )
}

export default Item;

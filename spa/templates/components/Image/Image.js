import React from 'react';
import { Img } from "@chakra-ui/react"


const Image = ({ ...props }) => {
  return <Img src={process.env.NEXT_PUBLIC_MGNL_DAM_RAW + props.image['@link']} alt={props.image['@name']} />;
};

export default Image;

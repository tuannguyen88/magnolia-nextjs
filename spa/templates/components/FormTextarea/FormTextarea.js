

import { convertBoolean } from '../../../services/util';
import {
  FormControl,
  FormLabel,
  Textarea,
  InputGroup
} from "@chakra-ui/react"

const FormEdit = (props) => {
  const id = "element" + props.metadata['@id'];
  const {label, require, disable, readonly, autocomplete, maxLength} = props;

  return (
    <FormControl id={id} mb={5}>
      {label &&
        <FormLabel fontWeight="normal">
          {label}
          {convertBoolean(require) && 
            <dfn title="required">*</dfn>
          }
        </FormLabel>
      }
      <InputGroup size="lg">
        <Textarea 
          placeholder={props.placeholder}
          isRequired={convertBoolean(require)}
          isDisabled={convertBoolean(disable)}
          isReadOnly={convertBoolean(readonly)}
          autoComplete={autocomplete}
          maxLength={maxLength ? maxLength*1 : ''}
        />
      </InputGroup> 
    </FormControl>
  );
}

export default FormEdit;
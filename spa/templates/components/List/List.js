import React from 'react';
import { EditableArea } from '@magnolia/react-editor';
import { List, OrderedList, UnorderedList } from "@chakra-ui/react"


const Listing = (props) => {
  const { items} = props;

  const listType = () => {
    switch(props.listType) {
      case "unordered":   
        return <UnorderedList>
          {items && <EditableArea content={items} parentTemplateId={props.metadata['mgnl:template']}/>}
        </UnorderedList>;
      case "ordered":   
        return <OrderedList>
          {items && <EditableArea content={items} parentTemplateId={props.metadata['mgnl:template']}/>}
        </OrderedList>;
      default: 
        return <List>
          {items && <EditableArea content={items} parentTemplateId={props.metadata['mgnl:template']}/>}
        </List>;
    }
  }

  return (
    <>
      {listType()}
    </>

  );
};

export default Listing;

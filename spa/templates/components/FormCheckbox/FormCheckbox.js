
import { useState } from 'react';
import { convertBoolean } from '../../../services/util';
import {
    FormControl,
    FormLabel,
    Stack,
    Checkbox,
    CheckboxGroup,
    Radio,
    RadioGroup

} from "@chakra-ui/react"

const FormCheckbox = (props) => {

    const id = "element" + props.metadata['@id'];
    const {label, require, disable, readonly, defaultValue} = props;

    const dataNodes = props.options['@nodes']
    let options = [];

    dataNodes.map((x)=>{
        options.push(props.options[x])
    })

    const checkboxType = () => {
        switch(props.type) {
            case "radio":
                return (

                    <RadioGroup
                        size="lg"
                        defaultValue={defaultValue}
                    >
                        <Stack spacing={2} direction={props.alignment}>
                            {options?.map(({ label, value, isDisable }) => {
                                return (
                                    <Radio size="lg" key={value} value={value} disabled={convertBoolean(isDisable)}>
                                        {label}
                                    </Radio>
                                )
                            })}
                        </Stack>

                    </RadioGroup>

                );
            default:
                return (

                    <CheckboxGroup
                        size="lg"
                        defaultValue={defaultValue}
                    >
                        <Stack spacing={2} direction={props.alignment}>
                            {options?.map(({ label, value, isDisable }) => {
                                return (
                                    <Checkbox size="lg" key={value} value={value} disabled={convertBoolean(isDisable)}>
                                        {label}
                                    </Checkbox>
                                )
                            })}
                        </Stack>
                    </CheckboxGroup>

                );
        }
    }

    return (
        <FormControl id={id} mb={5}>
            {label &&
            <FormLabel fontWeight="normal">
                {label}
                {convertBoolean(require) &&
                <dfn title="required">*</dfn>
                }
            </FormLabel>
            }
            {checkboxType()}

        </FormControl>
    );
}

export default FormCheckbox;

import { Box } from "@chakra-ui/react"

const Paragraph = props => <Box dangerouslySetInnerHTML={{ __html: props.richText }} />;

export default Paragraph;
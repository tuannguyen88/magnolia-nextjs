import React from 'react';
import { EditableArea, EditorContextHelper } from '@magnolia/react-editor';

class Expander extends React.Component {

  constructor(props) {
    super(props);
    this.state = {isCollapsed: false};
    this.toggle = this.toggle.bind(this);
  }

  state = {};
 
  toggle(event) {
    this.setState({
      isCollapsed: !this.state.isCollapsed
    });
    event.preventDefault();
  }

  componentDidUpdate() {
    
    // TODO: Need to comment out for now as it causes a warning in the page editor when used with Next.js
    // This means that if a user toggles an expander -it will not be editable.
    //EditorContextHelper.refresh();
    
  }

  render () {
    const expanderItems = this.props.expanderItems;
    return (
      <div className="expander">
        <div onClick={this.toggle} className={this.state.isCollapsed ? 'open expanderHeader d-flex' : 'closed expanderHeader d-flex'}>
          Expander
          {/* <svg className="expanderIcon" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"></path></svg> */}
        </div>
        
        {!this.state.isCollapsed &&
          <div>
            <div className="hint">[EXPANDER OPENED]</div>
            {expanderItems && <EditableArea content={expanderItems}  parentTemplateId={this.props.metadata['mgnl:template']}/>}
          </div>
        }   
      </div>
    );
  }
  

};

export default Expander;

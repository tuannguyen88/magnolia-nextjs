import {Heading, Box, Grid} from "@chakra-ui/react";
import EventItem from './EventItem';
import useSite from '../../../contexts/site';


const EventsList = (props) => {
	
	const {headline} = props;
	const {events} = useSite();
	
	
	return (
		<>
			<Heading
				as='h2'
				className='Headline'
			>
				{headline}
			</Heading>
			<Grid templateColumns="repeat(3, 1fr)" gap={6}>
				{events.length > 0 ? (
					
					events.map((event) => {
						return (
							<EventItem
								key={event['@id']}
								id={event['@id']}
								name={event['@name']}
								title={event.name}
								desc={event.description}
								location={event.location}
								date={event.date}
								image={event.imageLink.renditions["480x360"].link}
							/>
						)
					})
				
				) : (
					<Box>No List found</Box>
				)}
			</Grid>
		</>
	);
}

export default EventsList;
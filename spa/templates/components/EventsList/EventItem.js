import Link from 'next/link';

import {
	Box,
	Center,
	Heading,
	Text,
	Stack,
	Avatar,
	useColorModeValue,
} from '@chakra-ui/react';

import {humanReadableDate} from "../../../services/util";



const EventItem = (props) => {
	const {title, desc, location, date, image, name} = props;
	const eventDetailLink = `event/${name}`;
	const formatedDate = humanReadableDate(date);
	
	
	return (
		<Center py={6}>
			<Link
				href={eventDetailLink}
			>
				<Box
					as='a'
					maxW={'445px'}
					w={'full'}
					bg={useColorModeValue('white', 'gray.900')}
					boxShadow={'2xl'}
					rounded={'md'}
					p={6}
					overflow={'hidden'}
					cursor={'pointer'}
					className='EventItem'
					// _hover={{ cursor: "pointer" }}
					// _focus={{ boxShadow: "outline" }}
				>
					<Box
						h={'210px'}
						bg={'gray.100'}
						mt={-6}
						mx={-6}
						mb={6}
						pos={'relative'}
						backgroundImage={image}
						backgroundSize='100%'
						bgPosition='center'
						transition="all 0.2s cubic-bezier(.08,.52,.52,1)"
						_before={{
							content: `""`,
							position: "absolute",
							width: "100%",
							height: "100%",
							bg: "gray.900",
							opacity: "0.3",
							transition: 'all 0.2s'
						}}
						sx={{
							".EventItem:hover &": {
								backgroundSize: "200%",
							},
							".EventItem:hover &:before": {
								opacity: "0"
							}
						}}
					>
					
					</Box>
					<Stack>
						<Heading
							color={useColorModeValue('gray.700', 'white')}
							fontSize={'2xl'}
						>
							{title}
						</Heading>
						<Box
							dangerouslySetInnerHTML={{ __html: desc }}
							color={'gray.500'} noOfLines={2}
						/>
						
					</Stack>
					<Stack mt={6} direction={'row'} spacing={4} align={'center'}>
						<svg className="bi me-2" width="1em" height="1em">*/}
							<path fillRule="evenodd" d="M4 4a4 4 0 1 1 4.5 3.969V13.5a.5.5 0 0 1-1 0V7.97A4 4 0 0 1 4 3.999zm2.493 8.574a.5.5 0 0 1-.411.575c-.712.118-1.28.295-1.655.493a1.319 1.319 0 0 0-.37.265.301.301 0 0 0-.057.09V14l.002.008a.147.147 0 0 0 .016.033.617.617 0 0 0 .145.15c.165.13.435.27.813.395.751.25 1.82.414 3.024.414s2.273-.163 3.024-.414c.378-.126.648-.265.813-.395a.619.619 0 0 0 .146-.15.148.148 0 0 0 .015-.033L12 14v-.004a.301.301 0 0 0-.057-.09 1.318 1.318 0 0 0-.37-.264c-.376-.198-.943-.375-1.655-.493a.5.5 0 1 1 .164-.986c.77.127 1.452.328 1.957.594C12.5 13 13 13.4 13 14c0 .426-.26.752-.544.977-.29.228-.68.413-1.116.558-.878.293-2.059.465-3.34.465-1.281 0-2.462-.172-3.34-.465-.436-.145-.826-.33-1.116-.558C3.26 14.752 3 14.426 3 14c0-.599.5-1 .961-1.243.505-.266 1.187-.467 1.957-.594a.5.5 0 0 1 .575.411z"/>*/}
						</svg>
						<Stack direction={'column'} spacing={0} fontSize={'sm'}>
							<Text fontWeight={600}>{location}</Text>
							<Text color={'gray.500'}>{formatedDate}</Text>
						</Stack>
					</Stack>
				</Box>
			</Link>
		</Center>
	);
}

export default EventItem;
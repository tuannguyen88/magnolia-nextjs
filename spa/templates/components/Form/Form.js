import { EditableArea } from '@magnolia/react-editor';
import React, { useEffect } from 'react';
import { Heading, Box } from "@chakra-ui/react"

const Form = ({ onSubmit, ...props}) => {
  
  const fieldSets = props.fieldsets;

  useEffect(()=>{
    const handleEnter = (event) => {
      if (event.keyCode === 13 && event.target.nodeName === "INPUT") {
        const form = event.target.form;
        const elements = Array.from(form.elements).filter(el => {
          return el.nodeName === 'INPUT' && !el.disabled
        })
        
        const index = Array.prototype.indexOf.call(elements, event.target);
        const nextEl = elements[index + 1];
  
        if (nextEl) {
          elements[index + 1]?.focus();
        } else {
          onSubmit?.()
        }
        
        event.preventDefault();
      }
    }

    document.addEventListener("keydown", handleEnter);

    return () => {
      document.removeEventListener('keydown', handleEnter);
    }
  }, [onSubmit]);

  return (
    <div className="formWrapper" >
      <form 
        className='Form'
        onSubmit={onSubmit}
        name={props.formName}
      >
        { props.formTitle && 
          <Heading as='h2'>
            {props.formTitle}
          </Heading>
        }

        { props.formDescription && 
          <Box mb={5} dangerouslySetInnerHTML={{ __html: props.formDescription }} />
        }

        {fieldSets && <EditableArea content={fieldSets}  parentTemplateId={props.metadata['mgnl:template']}/>}
      </form>
    </div>
  );
}

export default Form;
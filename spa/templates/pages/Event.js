import React from 'react';
import { EditableArea } from '@magnolia/react-editor';
import {Heading} from "@chakra-ui/react";

function Event(props) {
  const { title, main, metadata } = props;

  return (
    <>
        <Heading lineHeight={'shorter'} as="h1" size="4xl" isTruncated >{title}</Heading>
        {main && <EditableArea content={main} parentTemplateId={metadata['mgnl:template']} />}
    </>
  );
}

export default Event;

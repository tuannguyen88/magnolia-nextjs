import React from 'react';
import { Container, Box } from "@chakra-ui/react";
import Navigation from '../Navigation';

// import './Header.scss';

const Header = (props) => {
  
  return (
    <Box as='header' className='Header' mb={3}>
      <Container maxW="container.xl">
        <Navigation/>
      </Container>
    </Box>
  );
};

export default Header;

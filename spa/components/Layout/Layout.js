import Header from '../Header';
import Footer from '../Footer';


const Layout = (props) => {
  const {children, indexPage } = props;
  const pageType = indexPage ? 'index-page' : 'logged-page';
  

  return (
    <div className={`${pageType} layout-container`}>
      <Header/>
      <main className="main">
        {children}
      </main>
      <Footer/>
    </div>
  )
}

export default Layout;
import {
	Box,
	Flex,
	Text,
	Icon,
	IconButton,
	Stack,
	Collapse,
	Popover,
	PopoverTrigger,
	PopoverContent,
	useColorModeValue,
	useBreakpointValue,
	useDisclosure,
} from "@chakra-ui/react";

import {
	HamburgerIcon,
	CloseIcon,
	ChevronDownIcon,
	ChevronRightIcon
} from "@chakra-ui/icons";

import NextLink from 'next/link'
import useSite from '../../contexts/site';


export function navItemsFilter() {
	const {navItems} = useSite();
	const [expectedNavWithoutRoot] = navItems.filter(x => x['@name'] == process.env.NEXT_PUBLIC_MGNL_APP_BASE.replace('/', ''));
	const items = expectedNavWithoutRoot['@nodes'].map((nodeMap) => {
		return expectedNavWithoutRoot[nodeMap];
	})
	return items.filter(x => x['hideInNav'] == 'false');
}

const DesktopNav = (navItems) => {
	
	const linkColor = useColorModeValue('gray.600', 'gray.200');
	const linkHoverColor = useColorModeValue('gray.800', 'white');
	const popoverContentBgColor = useColorModeValue('white', 'gray.800');
	
	return (
		<Stack direction={'row'} spacing={4}>
			{navItems.navItems.map((navItem) => (
				<Stack key={navItem.title}>
					<Popover trigger={'hover'} placement={'bottom-start'}>
						<PopoverTrigger>
							<Stack>
								<NextLink
									p={2}
									href={navItem['@path'] ?? '#'}
									fontSize={'sm'}
									fontWeight={500}
									color={linkColor}
									_hover={{
										textDecoration: 'none',
										color: linkHoverColor,
									}}>
									{navItem.title}
								</NextLink>
							</Stack>
						</PopoverTrigger>
						{navItem['@nodes'].length > 0 && (
							<PopoverContent
								border={0}
								boxShadow={'xl'}
								bg={popoverContentBgColor}
								p={4}
								rounded={'xl'}
								minW={'sm'}>
								<Stack>
									{navItem['@nodes'].map((i) => (
										<DesktopSubNav key={navItem[i]['@name']} {...navItem[i]} />
									))}
								</Stack>
							</PopoverContent>
						)}
					</Popover>
				</Stack>
			
			))}
		</Stack>
	);
};

const DesktopSubNav = (child) => {
	return (
		<NextLink
			href={child['@path'] ?? '#'}
		>
			<Stack
				direction={'row'}
				align={'center'}
				role={'group'}
				p={2}
				rounded={'md'}
				_hover={{bg: useColorModeValue('pink.50', 'gray.900')}}
			>
				
				<Text
					transition={'all .3s ease'}
					_groupHover={{color: 'pink.400'}}
					fontWeight={500}>
					{child.title}
				</Text>
				
				<Flex
					transition={'all .3s ease'}
					transform={'translateX(-10px)'}
					opacity={0}
					_groupHover={{opacity: '100%', transform: 'translateX(0)'}}
					justify={'flex-end'}
					align={'center'}
					flex={1}>
					<Icon color={'pink.400'} w={5} h={5} as={ChevronRightIcon}/>
				</Flex>
			</Stack>
		</NextLink>
	);
};

const MobileNav = (navItems) => {
	return (
		<Stack
			bg={useColorModeValue('white', 'gray.800')}
			p={4}
			display={{md: 'none'}}>
			{navItems.navItems.map((navItem) => (
				<MobileNavItem key={navItem.title} {...navItem} />
			))}
		</Stack>
	);
};

const MobileNavItem = (child) => {
	const {isOpen, onToggle} = useDisclosure();
	
	return (
		<Stack>
			<Flex
				py={2}
				justify={'space-between'}
				align={'center'}
				_hover={{
					textDecoration: 'none',
				}}
			>
				<NextLink
					href={child['@path'] ?? '#'}
				>
					<Text
						fontWeight={600}
						color={useColorModeValue('gray.600', 'gray.200')}>
						{child.title}
					</Text>
				</NextLink>
				{child['@nodes'].length > 0 && (
					<Icon
						as={ChevronDownIcon}
						transition={'all .25s ease-in-out'}
						transform={isOpen ? 'rotate(180deg)' : ''}
						w={6}
						h={6}
						onClick={onToggle}
					/>
				)}
			</Flex>
			<Collapse in={isOpen} animateOpacity mt={0}>
				<Stack
					mt={0}
					pl={4}
					borderLeft={1}
					borderStyle={'solid'}
					borderColor={useColorModeValue('gray.200', 'gray.700')}
					align={'start'}>
					{child['@nodes'].map((i) => (
						<NextLink
							href={child[i]['@path'] ?? '#'}
							key={child[i].title}
						>
							{child[i].title}
						</NextLink>
					))}
				</Stack>
			</Collapse>
		</Stack>
	);
};


const Navigation = () => {
	const navItems = navItemsFilter();
	const {isOpen, onToggle} = useDisclosure();
	
	return (
		<Box>
			<Flex
				bg={useColorModeValue('white', 'gray.800')}
				color={useColorModeValue('gray.600', 'white')}
				minH={'60px'}
				py={{base: 2}}
				px={{base: 4}}
				borderBottom={1}
				borderStyle={'solid'}
				borderColor={useColorModeValue('gray.200', 'gray.900')}
				align={'center'}>
				<Flex
					flex={{base: 1, md: 'auto'}}
					ml={{base: -2}}
					display={{base: 'flex', md: 'none'}}>
					<IconButton
						onClick={onToggle}
						icon={
							isOpen ? <CloseIcon w={3} h={3}/> : <HamburgerIcon w={5} h={5}/>
						}
						variant={'ghost'}
						aria-label={'Toggle Navigation'}
					/>
				</Flex>
				<Flex flex={{base: 1}} justify={{base: 'flex-end', md: 'start'}}>
					<Text
						textAlign={useBreakpointValue({base: 'center', md: 'left'})}
						fontFamily={'heading'}
						color={useColorModeValue('gray.800', 'white')}>
						Logo
					</Text>
					
					<Flex display={{base: 'none', md: 'flex'}} ml={10}>
						<DesktopNav navItems={navItems}/>
					</Flex>
				</Flex>
			</Flex>
			
			<Collapse in={isOpen} animateOpacity>
				<MobileNav navItems={navItems}/>
			</Collapse>
		</Box>
	);
}

export default Navigation;

import Head from 'next/head';

const NextHead = (props) => {
  
  return (
    <Head>
      <title>{props.title}</title>
      <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui" />
      <meta name="description" key="description" content={props.metaDescription} />
      <meta name="title" key="title" content={props.title} />
      <meta property="og:title" key="og:title" content={props.title} />
      <meta property="og:locale" key="og:locale" content="en_EU" />
      <meta property="og:type" key="og:type" content="website" />
      <meta property="og:description" key="og:description" content={props.metaDescription} />
      <link rel="icon" type="image/x-icon" href={`${process.env.NEXT_PUBLIC_ASSET_PREFIX}/static/favicon.ico`}/>
    </Head>
  );
};

export default NextHead;

import { useContext, createContext } from 'react';

export const SiteContext = createContext();

export function useSiteContext(data) {
  return {
    ...data,
  };
}

export default function useSite() {
  const site = useContext(SiteContext);
  return {
	  ...site,
	  events: site.Events,
	  navItems: site.navItems
  };
}
import App from 'next/app';
import {ChakraProvider, theme, CSSReset} from "@chakra-ui/react"
import {SiteContext, useSiteContext} from '../contexts/site';
import {getAllMenus, getEvents} from '../magnolia/api';


function MyApp({Component, pageProps = {}, navItems, Events}) {
	const site = useSiteContext({
		navItems, Events
	});
	
	return (
		<ChakraProvider theme={theme}>
			<CSSReset/>
			<SiteContext.Provider value={site}>
				<Component {...pageProps} />
			</SiteContext.Provider>
		</ChakraProvider>
	);
}

MyApp.getInitialProps = async function (appContext) {
	
	const appProps = await App.getInitialProps(appContext);
	const navItems = await getAllMenus();
	const Events = await getEvents();
	
	return {
		...appProps,
		navItems,
		Events
	};
};

export default MyApp;
import React from "react";
import {Container} from "@chakra-ui/react";
import {EditablePage} from '@magnolia/react-editor';

import NextHead from '../components/NextHead';
import Layout from '../components/Layout/Layout';

import {config} from '../magnolia/config';
import {getPage} from '../magnolia/api';

// export async function getStaticProps(context) {
//   console.log(context)
//   const page = await getPage(context.params.id);
//   return {
//     props: page,
//   };
// }


// export async function getStaticPaths() {
//   const pages = await fetch(
//     'http://localhost:8080/spa-project-webapp/.rest/delivery/pages/' 
//   );
//   const allPages = await pages.json();

//   const paths = allPages.results.map(
//     (path) => ({
//       params: { id: path['@path'] }
//     })
//   )

//   console.log(paths);

//   return {
//     paths, 
//     fallback: false
//   };
// }


export async function getServerSideProps(context) {
	const page = await getPage(context);
	return {
		props: page,
	};
}


const Page = (props) => {
	
	return (
		<Layout indexPage>
			<NextHead
				title={props.page.title || ''}
				metaDescription={props.page.description || ''}
			/>
			<Container maxW="container.xl">
				{props.page &&
				<EditablePage content={props.page} config={config} templateDefinitions={props.templateDefinitions}/>}
			</Container>
		</Layout>
	)
}

export default Page;
